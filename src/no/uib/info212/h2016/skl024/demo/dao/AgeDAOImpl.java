package no.uib.info212.h2016.skl024.demo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import no.uib.info212.h2016.skl024.demo.dao.interfaces.AgeDAO;
import no.uib.info212.h2016.skl024.demo.models.Age;

public class AgeDAOImpl implements AgeDAO {

	
	Connection connection = null;
    PreparedStatement statement = null;
    ResultSet resultSet = null;
	
    String url = "jdbc:mysql://localhost:3306/test";
    String user = "root";
    String password = "";
	
    String insertNameAndAge = "INSERT INTO tabletest"
			+ "(text, number)" + " VALUES(?,?)";
    
    String getAllNameAndAge = "SELECT * FROM tabletest";
	
	public AgeDAOImpl() {
		super();
	}

	@Override
	public void connect() {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			connection = DriverManager.getConnection(url, user, password);
					
			

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void writeName(String name, int age) {
		
		try {
			statement = connection.prepareStatement(insertNameAndAge);	
			statement.setString(1, name);
			statement.setInt(2, age);
			statement.execute();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public List<Age> getAllAges() {
		
		List<Age> result = new LinkedList<Age>();
		
		try {
			statement = connection.prepareStatement(getAllNameAndAge);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				System.out.println("I happened!");
				result.add(new Age(resultSet.getInt(2), resultSet.getString(1)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return result;
	}

	@Override
	public List<Age> getAge(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Age> getNames(int age) {
		// TODO Auto-generated method stub
		return null;
	}

}
