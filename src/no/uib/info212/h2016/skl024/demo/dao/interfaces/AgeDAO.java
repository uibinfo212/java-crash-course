package no.uib.info212.h2016.skl024.demo.dao.interfaces;

import java.util.List;

import no.uib.info212.h2016.skl024.demo.models.Age;

public interface AgeDAO {
	
	void connect();
	
	List<Age> getAge(String name);
	
	List<Age> getNames(int age);
	
	

}
