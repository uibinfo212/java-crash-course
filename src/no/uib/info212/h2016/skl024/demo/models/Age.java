package no.uib.info212.h2016.skl024.demo.models;

public class Age {
	
	private int age;
	private String name;
	
	
	public Age(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	

}

