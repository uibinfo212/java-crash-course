package no.uib.info212.h2016.skl024.demo;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI extends JFrame {

	public static GUI instance; 
	
	private static final long serialVersionUID = 1L;

	private JPanel currentView;

	private JPanel previousView;
	
	private GUI() {
		
		System.out.println("I happened!");
	}
	
	public static GUI getInstance() {
		if (GUI.instance == null) {
			GUI.instance = new GUI(); 
		}
		
		return GUI.instance; 
	}
	
	public void initializeGUI() {
		
		setTitle("Java Crash Course");
        setPreferredSize(new Dimension(300, 200));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
	}

	public JPanel getCurrentView() {
		return currentView;
	}

	public void setCurrentView(JPanel newView) {
		this.previousView = this.currentView;
		this.currentView = newView;
	}
	
	public void changeView() {
		if (previousView != null) {
			this.remove(previousView);
			previousView = null;
		}
		this.add(currentView);
		this.pack();
		this.repaint();
	}
	
	
	

}




