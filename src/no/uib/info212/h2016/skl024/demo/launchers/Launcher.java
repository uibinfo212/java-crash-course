package no.uib.info212.h2016.skl024.demo.launchers;

import no.uib.info212.h2016.skl024.demo.Controller;

public class Launcher {
	
	public static void main(String[] args) {
		System.out.println("HELLO!");
		Controller app = new Controller(); 
		
		// This change is useless. 
	}

}
