package no.uib.info212.h2016.skl024.demo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.uib.info212.h2016.skl024.demo.dao.AgeDAOImpl;
import no.uib.info212.h2016.skl024.demo.models.Age;

public class Controller implements ActionListener{
	
	
	private GUI gui;
	
	private AgeDAOImpl ages; 
	
	private JPanel viewOne;
	private JPanel viewTwo;
	private JPanel viewThree;
	
	private JTextField textName;
	private JTextField textAge;
	
	
	public Controller() {
		System.out.println("I happened!");
		gui = GUI.getInstance();
		gui.initializeGUI();
		
		ages = new AgeDAOImpl();
		ages.connect();
		
		viewOne = createViewOne();
		viewTwo = createViewTwo();
		viewThree = createViewThree();
		
		insertPanel(viewOne);
		
		addMenubar();
		
		gui.setVisible(true);
		
		
	}
	

	public void insertPanel(JPanel panel) {
		
		gui.setCurrentView(panel);
		gui.changeView();
		
	}
	
	
	public JPanel createViewOne() {
		JPanel panel = new JPanel();
		
		JButton quitButton = new JButton("Blue");
	    quitButton.addActionListener(this);
		
	    JButton execute = new JButton("Write");
	    execute.addActionListener(this);
	    
	    textName = new JTextField();
	    textName.setPreferredSize(new Dimension(100, 20));
	    textAge = new JTextField();
	    textAge.setPreferredSize(new Dimension(40, 20));
	    
	   
	    panel.setBackground(Color.RED);
	    panel.add(quitButton);
	    
	    panel.add(textName);
	    panel.add(textAge);
	    panel.add(execute);
	    
	    
		
		return panel;
	}
	
	public JPanel createViewTwo() {
		JPanel panel = new JPanel();
		
		JButton forwardButton = new JButton("Green");
		forwardButton.addActionListener(this);
		
		String allAges = "<html>";
		
		for (Age age : ages.getAllAges()) {
			
			allAges += age.getName() + ", " + age.getAge() + " <br> ";
		}
		
		allAges += "</html>";
		JLabel wallOfText = new JLabel(allAges);
		
		panel.setBackground(Color.BLUE);
		panel.add(forwardButton);
		panel.add(wallOfText);
		
		return panel;
	}
	
	private JPanel createViewThree() {
		JPanel panel = new JPanel();
		JButton backButton = new JButton("Red Buttons");
		
		backButton.addActionListener(this);
		
		panel.setBackground(Color.GREEN);
		panel.add(backButton);
		
		return panel;
	}
	
	private void addMenubar(){
		
		JMenuBar menubar = new JMenuBar();
		
		JMenu file = new JMenu("File");
		
		JMenu views = new JMenu ("Views");
		
		JMenuItem exit = new JMenuItem("Exit");
		exit.addActionListener(this);
		
		JMenuItem menuViewOne = new JMenuItem("View One");
		menuViewOne.addActionListener(this);
		
		JMenuItem menuViewTwo = new JMenuItem("View Two");
		menuViewTwo.addActionListener(this);
		
		JMenuItem menuViewThree = new JMenuItem("View Three");
		menuViewThree.addActionListener(this);
		
		file.add(exit);
		views.add(menuViewOne); 
		views.add(menuViewTwo);
		views.add(menuViewThree);
		
		menubar.add(file);
		menubar.add(views);
		
		gui.setJMenuBar(menubar);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() instanceof JButton) {
			
			JButton source = (JButton) e.getSource();
		
			if (source.getText().equals("Blue")) {
				insertPanel(viewTwo);
			} else if (source.getText().equals("Green")) {
				insertPanel(viewThree);
			} else if (source.getText().equals("Red")) {
				insertPanel(viewOne);
			} else if (source.getText().equals("Write")) {
				Age age = createAge();
				ages.writeName(age.getName(), age.getAge());
			}
		
		} else if (e.getSource() instanceof JMenuItem){
			
			JMenuItem source = (JMenuItem) e.getSource();
			
			if (source.getText().equalsIgnoreCase("Exit")) {
				System.exit(0);
			} else if (source.getText().equalsIgnoreCase("View One")) {
				insertPanel(viewOne);
			} else if (source.getText().equalsIgnoreCase("View Two")) {
				insertPanel(viewTwo);
			} else if (source.getText().equalsIgnoreCase("View Three")) {
				insertPanel(viewThree);
			}
			
		}
		
	}


	private Age createAge() {
		
		String name = textName.getText();
		int age = Integer.parseInt(textAge.getText());
		
		Age result = new Age(age, name);
		
		return result;
	}

}



